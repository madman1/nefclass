﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NEFClassLib
{
    public class Rule
    {
        private int[] _antecedents;
        private int _resultClass;

        public Rule(int[] antecedents, int resultClass)
        {
            _antecedents = new int[antecedents.Length];
            for (int i = 0; i < antecedents.Length; ++i)
                _antecedents[i] = antecedents[i];

            _resultClass = resultClass;
        }

        public int[] Antecedents
        {
            get { return _antecedents; }
        }

        public int ResultClass
        {
            get { return _resultClass; }
            set { _resultClass = value; }
        }
    }
}
