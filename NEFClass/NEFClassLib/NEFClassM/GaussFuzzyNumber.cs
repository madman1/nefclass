﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NEFClassLib
{
    class GaussFuzzyNumber
    {
        private const double MIN_WIDTH = 0.1;

        private double _a;
        private double _b;

        private Boolean mLeftShouldered;
        private Boolean mRightShouldered;

        public GaussFuzzyNumber(double a, double b, bool leftShouldered, bool rightShouldered)
        {
            _a = a;
            _b = b;

            mLeftShouldered = leftShouldered;
            mRightShouldered = rightShouldered;
        }

		public double A
		{
			get { return _a; }
			set { _a = value; }
		}

		public double B
		{
			get { return _b; }
			set { _b = value; }
		}

        public double GetMembership(double x)
        {
            if (mLeftShouldered && x <= _a)
                return 1;

            if (mRightShouldered && x >= _a)
                return 1;

            return Math.Exp(-(x - _a) * (x - _a) / (2 * _b * _b));
        }

        public void Adapt(double deltaA, double deltaB)
        {
            _a += deltaA;

            if (_b + deltaB <= 0)
                deltaB = 0;
            _b += deltaB;
        }
    }
}
