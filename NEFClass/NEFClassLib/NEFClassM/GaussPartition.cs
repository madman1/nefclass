﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NEFClassLib
{
    class GaussPartition
    {
        private Bounds _bounds;
        private GaussFuzzyNumber[] _fuzzyParts;

        public GaussPartition(Bounds bounds, int fuzzyPartsCount)
        {
            _bounds = bounds;
            _fuzzyParts = new GaussFuzzyNumber[fuzzyPartsCount];

            double b1 = (bounds.MaxValue - bounds.MinValue) / (fuzzyPartsCount + 1);
            double b = b1 / Math.Sqrt(2 * Math.Log(2));
            for (int i = 0; i < fuzzyPartsCount; ++i)
                _fuzzyParts[i] = new GaussFuzzyNumber(bounds.MinValue + (i + 1) * b1, b, i == 0, i == fuzzyPartsCount - 1);
        }

		public int PartitionSize
		{
			get { return _fuzzyParts.Length; }
		}

		public GaussFuzzyNumber this[int index]
		{
			get { return _fuzzyParts[index]; }
		}

        public double[] GetMembershipVector(double x)
        {
            int size = _fuzzyParts.Length;

            double[] result = new double[size];
            for (int i = 0; i < size; ++i)
                result[i] = _fuzzyParts[i].GetMembership(x);
            
            return result;
        }

        public double GetMaxMembership(double x)
        {
            double result = 0.0;

            int size = _fuzzyParts.Length;
            for (int i = 0; i < size; ++i)
                result = Math.Max(_fuzzyParts[i].GetMembership(x), result);

            return result;
        }

        public int GetMaxMembershipIndex(double x)
        {
            int maxIndex = 0;

            int size = _fuzzyParts.Length;
            for (int i = 1; i < size; ++i)
                if (_fuzzyParts[i].GetMembership(x) > _fuzzyParts[maxIndex].GetMembership(x))
                    maxIndex = i;

            return maxIndex;
        }

        public void Adapt(int index, double deltaA, double deltaB)
        {
            _fuzzyParts[index].Adapt(deltaA, deltaB);
        }
    }
}
