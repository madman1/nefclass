﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NEFClassLib
{
    public struct Bounds
    {
        private double _minValue;
        private double _maxValue;

        public Bounds(double min, double max)
        {
            _minValue = min;
            _maxValue = max;
        }

        public double MinValue
        {
            get { return _minValue; }
            set { _minValue = value; }
        }
        public double MaxValue
        {
            get { return _maxValue; }
            set { _maxValue = value; }
        }
    }
}
