﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NEFClassLib
{
	/// <summary>
	/// Contains numerical bounds of a parameter and fuzzy parts of parameter
	/// </summary>
    public class Partition
    {
        private Bounds _bounds;
        private TriangleFuzzyNumber[] _fuzzyParts;

        public Partition(Bounds bounds, int fuzzyPartsCount)
        {
            _bounds = bounds;
            _fuzzyParts = new TriangleFuzzyNumber[fuzzyPartsCount];

            double width = (bounds.MaxValue - bounds.MinValue) / (fuzzyPartsCount + 1);
            for (int i = 0; i < fuzzyPartsCount; ++i)
                _fuzzyParts[i] = new TriangleFuzzyNumber(bounds.MinValue + i * width, bounds.MinValue + (i + 1) * width, bounds.MinValue + (i + 2) * width, i == 0, i == fuzzyPartsCount - 1);
        }

        public double[] GetMembershipVector(double x)
        {
            int size = _fuzzyParts.Length;

            double[] result = new double[size];
            for (int i = 0; i < size; ++i)
                result[i] = _fuzzyParts[i].GetMembership(x);
            
            return result;
        }

        public double GetMaxMembership(double x)
        {
            double result = 0.0;

            int size = _fuzzyParts.Length;
            for (int i = 0; i < size; ++i)
                result = Math.Max(_fuzzyParts[i].GetMembership(x), result);

            return result;
        }

        public int GetMaxMembershipIndex(double x)
        {
            int maxIndex = 0;

            int size = _fuzzyParts.Length;
            for (int i = 1; i < size; ++i)
                if (_fuzzyParts[i].GetMembership(x) > _fuzzyParts[maxIndex].GetMembership(x))
                    maxIndex = i;

            return maxIndex;
        }

        public void Adapt(int index, double deltaA, double deltaB, double deltaC)
        {
            if ((_fuzzyParts[index].Left + deltaA) < _bounds.MinValue) deltaA = 0.0;
            if ((_fuzzyParts[index].Right + deltaC) > _bounds.MaxValue) deltaC = 0.0; 
            
            _fuzzyParts[index].Adapt(deltaA, deltaB, deltaC);
        }

        public int PartitionSize
        {
            get { return _fuzzyParts.Length; }
        }

        public TriangleFuzzyNumber this[int index]
        {
            get { return _fuzzyParts[index]; }
        }
    }
}
