﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NEFClassLib
{
	// http://whatis.techtarget.com/definition/fuzzy-number
    public class TriangleFuzzyNumber
    {
        private const double MIN_WIDTH = 0.1;

        private double _left;   
        private double _mainValue;
        private double _right;

        private Boolean mLeftShouldered;
        private Boolean mRightShouldered;

        public TriangleFuzzyNumber(double left, double main, double right, bool leftShouldered, bool rightShouldered)
        {
            _left = left;
            _mainValue = main;
            _right = right;

            mLeftShouldered = leftShouldered;
            mRightShouldered = rightShouldered;
        }

		public double Left
		{
			get { return _left; }
			set { _left = value; }
		}

		public double Right
		{
			get { return _right; }
			set { _right = value; }
		}

		public double MainValue
		{
			get { return _mainValue; }
			set { _mainValue = value; }
		}

        public double GetMembership(double x)
        {
            if (mLeftShouldered && x <= _mainValue)
                return 1;

            if (mRightShouldered && x >= _mainValue)
                return 1;

            if (x <= _left || x >= _right)
                return 0;

            if (x <= _mainValue)
                return (x - _left) / (_mainValue - _left);
            else
                return (_right - x) / (_right - _mainValue);
        }

        public void Adapt(double deltaA, double deltaB, double deltaC)
        {
            if (mLeftShouldered || (_left + deltaA) > (_mainValue - MIN_WIDTH)) deltaA = 0.0;
            if (mRightShouldered || (_right + deltaC) < (_mainValue + MIN_WIDTH)) deltaC = 0.0;
            if ((_left + deltaA + MIN_WIDTH) > (_mainValue + deltaB) ||
                (_right + deltaC - MIN_WIDTH) < (_mainValue + deltaB)) deltaB = 0.0; 

            _left += deltaA;
            _mainValue += deltaB;
            _right += deltaC;
        }
    }
}
