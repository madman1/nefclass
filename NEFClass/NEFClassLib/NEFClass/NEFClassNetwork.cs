﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logging;

namespace NEFClassLib
{
    public class NEFClassNetwork
    {
        private const string LOG_TAG = "NEFClassNetwork";

        private double[] _inputLayer;
        private double[] _hiddenLayer;
        private double[] _outputLayer;

        private int[] _minAncedent;

        private Partition[] _partitions;
        private Rule[] _rules;
        private string[] _classNames;

        public NEFClassNetwork(NCDataSet trainDataset, TrainConfiguration trainConfig)
        {
            Log.LogMessage(LOG_TAG, "Начало обучения.");

			// Build network
            _classNames = trainDataset.GetClassesList();

            _inputLayer = new double[trainDataset.Dimension];
            _outputLayer = new double[_classNames.Length];

            _partitions = new Partition[trainDataset.Dimension];
            for (int i = 0; i < trainDataset.Dimension; ++i)
                _partitions[i] = new Partition(trainDataset.GetAttributeBounds(i), trainConfig.FuzzyPartsCount[i]);

			// Generating base of rules
            CreateRulesBase(trainDataset, trainConfig);
            _hiddenLayer = new double[_rules.Length];
            _minAncedent = new int[_rules.Length];

			// Train fuzzy sets
            if (trainConfig.DoOptimization)
                TrainRules(trainDataset, trainConfig);
        }

		public int GetClassIndex(string cls)
		{
			for (int i = 0; i < _classNames.Length; ++i)
				if (_classNames[i] == cls)
					return i;

			return -1;
		}

		public string Classify(NCEntity entity)
		{
			Propagate(entity);

			int resultClass = 0;
			for (int i = 0; i < _outputLayer.Length; ++i)
				if (_outputLayer[i] > _outputLayer[resultClass])
					resultClass = i;

			if (_outputLayer[resultClass] > 0)
				return GetClassName(resultClass);

			return "";
		}

		public double[] GetOutput(NCEntity entity)
		{
			Propagate(entity);
			return _outputLayer;
		}

		#region Private Methods

		private string GetClassName(int index)
		{
			return _classNames[index];
		}

		private void TrainRules(NCDataSet trainDataset, TrainConfiguration trainConfig)
        {
            int iteration = 0;
            double err = 0, prevErr = 0;
            do
            {
                prevErr = err;
                err = 0;
                int misClassed = 0;
                for (int i = 0; i < trainDataset.Length; ++i)
                {
                    bool isCorrect = true;
                    double patternError = 0;
                    AdaptByPattern(trainDataset[i], trainConfig, out patternError, out isCorrect);

                    err += patternError;
                    if (!isCorrect)
                        ++misClassed;
                }

                int misClassedFinal = 0;
                for (int i = 0; i < trainDataset.Length; ++i)
                {
                    string result = Classify(trainDataset[i]);
                    bool isCorrect = (result == trainDataset[i].Class);

                    if (!isCorrect)
                        ++misClassedFinal;

                    double[] targetOutput = new double[_outputLayer.Length];
                    targetOutput[GetClassIndex(trainDataset[i].Class)] = 1.0;

                    for (int z = 0; z < _outputLayer.Length; ++z)
                        err += (_outputLayer[z] - targetOutput[z]) * (_outputLayer[z] - targetOutput[z]);
                }

                err /= trainDataset.Length;
                if ((iteration + 1) % 10 == 0)
                    Log.LogMessage(LOG_TAG, "It: {0}. Error: {1}, MisClassed: {2}", iteration + 1, err.ToString("0.######"), misClassedFinal);
            } while (++iteration <= trainConfig.MaxIterations && Math.Abs(err - prevErr) > trainConfig.Accuracy);
        }

        private void CreateRulesBase(NCDataSet trainDataset, TrainConfiguration trainConfig)
        {
            if (trainConfig.RulesTrainAlgo == TrainConfiguration.TRAIN_RULES_SIMPLE)
                _rules = CreateRulesSimple(trainDataset, trainConfig);
            else if (trainConfig.RulesTrainAlgo == TrainConfiguration.TRAIN_RULES_BEST)
                _rules = CreateRulesBest(trainDataset, trainConfig);
            else if (trainConfig.RulesTrainAlgo == TrainConfiguration.TRAIN_RULES_BEST_FOR_CLASS)
                _rules = CreateRulesBestPerClass(trainDataset, trainConfig);
            else
                throw new ArgumentException("Wrong rules train algorithm: " + trainConfig.RulesTrainAlgo);
        }

        private Rule[] CreateRulesFromDataset(NCDataSet trainDataset, int maxRules)
        {
            List<Rule> rules = new List<Rule>();

            for (int i = 0; i < trainDataset.Length; ++i)
            {
                if (maxRules > 0 && rules.Count >= maxRules)
                    break;

                NCEntity entity = trainDataset[i];

                int[] maxFS = new int[entity.Dimension];
                for (int j = 0; j < entity.Dimension; ++j)
                    maxFS[j] = _partitions[j].GetMaxMembershipIndex(entity[j]);

                bool found = false;
                for (int k = 0; k < rules.Count; ++k)
                {
                    bool equal = true;
                    for (int z = 0; z < rules[k].Antecedents.Length; ++z)
                        equal = equal && (rules[k].Antecedents[z] == maxFS[z]);

                    if (equal)
                    {
                        found = true;
                        break;
                    }
                }

                if (found)
                    continue;

                rules.Add(new Rule(maxFS, GetClassIndex(entity.Class)));
                Log.LogMessage(LOG_TAG, "Образец {0} добавил правило {1}", i + 1, rules.Count);
            }

            return rules.ToArray();
        }

        private Rule[] CreateRulesSimple(NCDataSet trainDataset, TrainConfiguration trainConfig)
        {
            Log.LogMessage(LOG_TAG, "Генерация базы правил по алгоритму: simple");
            Rule[] rules = CreateRulesFromDataset(trainDataset, trainConfig.RuleNodesMax);
            Log.LogMessage(LOG_TAG, "База правил сгенерирована.");

            return rules;
        }

        private void CheckRulesConnections(Rule[] rules, NCDataSet checkDataset)
        {
            double[,] activations = new double[rules.Length, _classNames.Length];

            for (int i = 0; i < checkDataset.Length; ++i)
            {
                NCEntity entity = checkDataset[i];

                for (int r = 0; r < rules.Length; ++r)
                {
                    double activation = 1;
                    for (int j = 0; j < entity.Dimension; ++j)
                        activation = Math.Min(activation, _partitions[j].GetMembershipVector(entity[j])[rules[r].Antecedents[j]]);

                    int entityClass = GetClassIndex(entity.Class);
                    activations[r, entityClass] += activation;
                }
            }

            for (int r = 0; r < rules.Length; ++r)
            {
                int maxActivationIndex = 0;
                for (int k = 1; k < _classNames.Length; ++k)
                    if (activations[r, maxActivationIndex] < activations[r, k])
                        maxActivationIndex = k;

                if (activations[r, rules[r].ResultClass] < activations[r, maxActivationIndex])
                {
                    Log.LogMessage(LOG_TAG, "Следствие правила {0} изменено с {1} на {2}", r, rules[r].ResultClass, maxActivationIndex);
                    rules[r].ResultClass = maxActivationIndex;
                }
            }
        }

        private double[] CalculateRulesPerformance(Rule[] rules, NCDataSet checkDataset)
        {
            double[] performance = new double[rules.Length];

            for (int i = 0; i < checkDataset.Length; ++i)
            {
                NCEntity entity = checkDataset[i];

                for (int r = 0; r < rules.Length; ++r)
                {
                    double activation = 1;
                    for (int j = 0; j < entity.Dimension; ++j)
                        activation = Math.Min(activation, _partitions[j].GetMembershipVector(entity[j])[rules[r].Antecedents[j]]);
                    
                    performance[r] += activation * (rules[r].ResultClass == GetClassIndex(entity.Class) ? 1 : -1);
                }
            }

            return performance;
        }

        private Rule[] CreateRulesBest(NCDataSet trainDataset, TrainConfiguration trainConfig)
        {
            Log.LogMessage(LOG_TAG, "Генерация базы правил по алгоритму: best");
            Rule[] rules = CreateRulesFromDataset(trainDataset, 0);

            CheckRulesConnections(rules, trainDataset);

            double[] rulesPerformance = CalculateRulesPerformance(rules, trainDataset);
            double minPerformance = -trainDataset.Length - 10;

            List<Rule> resultRules = new List<Rule>();
            for (int i = 0; i < trainConfig.RuleNodesMax; ++i)
            {
                int ruleToSelect = 0;
                for (int j = 1; j < rules.Length; ++j)
                    if (rulesPerformance[ruleToSelect] < rulesPerformance[j])
                        ruleToSelect = j;

                if (rulesPerformance[ruleToSelect] != minPerformance)
                {
                    resultRules.Add(rules[ruleToSelect]);
                    rulesPerformance[ruleToSelect] = minPerformance;
                }
            }

            Log.LogMessage(LOG_TAG, "Отобрано {0} лучших правил.", resultRules.Count);
            Log.LogMessage(LOG_TAG, "База правил сгенерирована.");
            
            return resultRules.ToArray();
        }

        private Rule[] CreateRulesBestPerClass(NCDataSet trainDataset, TrainConfiguration trainConfig)
        {
            Log.LogMessage(LOG_TAG, "Генерация базы правил по алгоритму: best per class");
            Rule[] rules = CreateRulesFromDataset(trainDataset, 0);

            CheckRulesConnections(rules, trainDataset);

            double[] rulesPerformance = CalculateRulesPerformance(rules, trainDataset);
            double minPerformance = -trainDataset.Length - 10;

            List<Rule> resultRules = new List<Rule>();

            for (int i = 0; i < _classNames.Length; ++i)
                for (int z = 0; z < trainConfig.RuleNodesMax / _classNames.Length; ++z)
                {
                    int ruleToSelect = 0;
                    while (rules[ruleToSelect].ResultClass != i) ++ruleToSelect;

                    for (int j = 0; j < rules.Length; ++j)
                        if (rulesPerformance[ruleToSelect] < rulesPerformance[j] && rules[j].ResultClass == i)
                            ruleToSelect = j;

                    if (rulesPerformance[ruleToSelect] != minPerformance)
                    {
                        resultRules.Add(rules[ruleToSelect]);
                        rulesPerformance[ruleToSelect] = minPerformance;
                    }
                }

            int alreadySelected = resultRules.Count;
            for (int i = 0; i < trainConfig.RuleNodesMax - alreadySelected; ++i)
            {
                int ruleToSelect = 0;
                for (int j = 0; j < rules.Length; ++j)
                    if (rulesPerformance[ruleToSelect] < rulesPerformance[j])
                        ruleToSelect = j;

                if (rulesPerformance[ruleToSelect] != minPerformance)
                {
                    resultRules.Add(rules[ruleToSelect]);
                    rulesPerformance[ruleToSelect] = minPerformance;
                }
            }

            Log.LogMessage(LOG_TAG, "Отобрано {0} лучших правил.", resultRules.Count);
            Log.LogMessage(LOG_TAG, "База правил сгенерирована.");

            return resultRules.ToArray();
        }

        private void Propagate(NCEntity entity)
        {
            for (int i = 0; i < entity.Dimension; ++i)
                _inputLayer[i] = entity[i];

            double[][] membership = new double[entity.Dimension][];
            for (int i = 0; i < entity.Dimension; ++i)
                membership[i] = _partitions[i].GetMembershipVector(entity[i]);

            for (int i = 0; i < _rules.Length; ++i)
            {
                double activation = 1;
                for (int j = 0; j < _inputLayer.Length; ++j)
                    if (membership[j][_rules[i].Antecedents[j]] < activation)
                    {
                        activation = membership[j][_rules[i].Antecedents[j]];
                        _minAncedent[i] = j;
                    }

                _hiddenLayer[i] = activation;
            }

            for (int i = 0; i < _outputLayer.Length; ++i)
            {
                _outputLayer[i] = 0;
                for (int r = 0; r < _rules.Length; ++r)
                {
                    if (_rules[r].ResultClass != i)
                        continue;

                    _outputLayer[i] = Math.Max(_outputLayer[i], _hiddenLayer[r]);
                }
            }
        }
        
        private void AdaptByPattern(NCEntity pattern, TrainConfiguration trainConfig, out double error, out bool isCorrect)
        {
            Propagate(pattern);

            int maxIndex = 0;
            for (int i = 0; i < _outputLayer.Length; ++i)
                if (_outputLayer[i] > _outputLayer[maxIndex])
                    maxIndex = i;

            isCorrect = (GetClassName(maxIndex) == pattern.Class) && (_outputLayer[maxIndex] > 0);

            double[] targetOutput = new double[_outputLayer.Length];
            double[] deltaOut = new double[_outputLayer.Length];

            targetOutput[GetClassIndex(pattern.Class)] = 1;
            for (int i = 0; i < _outputLayer.Length; ++i)
                deltaOut[i] = targetOutput[i] - _outputLayer[i];
            
            for (int r = 0; r < _rules.Length; ++r)
            {
                if (_hiddenLayer[r] > 0)
                {
                    double[] deltaHidden = new double[_hiddenLayer.Length];
                    deltaHidden[r] = deltaOut[_rules[r].ResultClass] * _hiddenLayer[r] * (1 - _hiddenLayer[r]);

                    int minAncedent = _minAncedent[r];
                    TriangleFuzzyNumber fuzzyPart = _partitions[minAncedent][_rules[r].Antecedents[minAncedent]];

                    double factor = deltaHidden[r] * (fuzzyPart.Right - fuzzyPart.Left);
                    double deltaB = trainConfig.OptimizationSpeed * factor * Math.Sign(_inputLayer[minAncedent] - fuzzyPart.MainValue);
                    double deltaA = -trainConfig.OptimizationSpeed * factor + deltaB;
                    double deltaC = trainConfig.OptimizationSpeed * factor + deltaB;

                    _partitions[minAncedent].Adapt(_rules[r].Antecedents[minAncedent], deltaA, deltaB, deltaC);
                }
            }

            error = 0.0;
            for (int i = 0; i < _outputLayer.Length; ++i)
                error += deltaOut[i] * deltaOut[i];
        }

		#endregion
    }
}
