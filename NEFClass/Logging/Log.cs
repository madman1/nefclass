﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logging
{
    public interface ILogHandler
    {
        void HandleMessage(string tag, string message);
    }

    public static class Log
    {
		// observer pattern is used
        private static List<ILogHandler> _handlers = new List<ILogHandler>();

        public static void RegisterHandler(ILogHandler handler)
        {
            if (!_handlers.Contains(handler))
                _handlers.Add(handler);
        }

        public static void UnregisterHandler(ILogHandler handler)
        {
            _handlers.Remove(handler);
        }

        public static void LogMessage(string tag, string message, params object[] args)
        {
            for (int i = 0; i < _handlers.Count; ++i)
                _handlers[i].HandleMessage(tag, String.Format(message, args));
        }
    }
}
