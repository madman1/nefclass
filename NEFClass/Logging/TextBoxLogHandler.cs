﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Logging
{
    public class TextBoxLogHandler: ILogHandler
    {
        private TextBox _textBox;

        public TextBoxLogHandler(TextBox textBox)
        {
            _textBox = textBox;
        }

        public void HandleMessage(string tag, string message)
        {
            _textBox.AppendText(tag + ": " + message + Environment.NewLine);
        }
    }
}
